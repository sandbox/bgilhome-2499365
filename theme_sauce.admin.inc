<?php

function theme_sauce_settings() {
  $form = array();

  // Template files options
  $form['tpl'] = array(
    '#type' => 'fieldset',
    '#title' => t('Template files'),
  );
  // page, per content type
  $form['tpl']['ts_tpl_content_type'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable page tpl files per content type'),
    '#description' => t('Provides page--$type.tpl.php'),
    '#default_value' => variable_get('ts_tpl_content_type'),
  );
  /// page, for error pages
  $form['tpl']['ts_tpl_error_pages'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable page tpl files for error pages'),
    '#description' => t('Provides page--$httpcode.tpl.php'),
    '#default_value' => variable_get('ts_tpl_error_pages'),
  );
  // node, per view mode
  $form['tpl']['ts_tpl_view_mode'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable node tpl files per view mode'),
    '#description' => t('Provides node--$viewmode.tpl.php, node--$type--$viewmode.tpl.php'),
    '#default_value' => variable_get('ts_tpl_view_mode'),
  );
  // per theme registry key
  // get theme registry keys that don't already have templates provided
  // $theme_registry = theme_get_registry();
  // $theme_keys_no_tpl = array_keys(array_filter($theme_registry, function ($v) { return !empty($v['template']) && empty($v['ts']); }));
  // $options = array_combine($theme_keys_no_tpl, $theme_keys_no_tpl);
  // ksort($options);
  // EDIT: only provide options for a few commonly overridden elements
  // $options = array(
  //   'form_element',
  // );
  // $options = array_combine($options, $options);
  // ksort($options);
  // $form['tpl']['ts_tpl_theme_hooks'] = array(
  //   '#type' => 'checkboxes',
  //   '#title' => t('Enable tpl files for (untemplated) theme registry keys'),
  //   '#description' => t('Provides $themekey.tpl.php, eg. form-element.tpl.php'),
  //   '#default_value' => variable_get('ts_tpl_theme_hooks', array()),
  //   '#options' => $options,
  // );

  // Preprocess options
  $form['pp'] = array(
    '#type' => 'fieldset',
    '#title' => t('Preprocess variables'),
  );
  $form['pp']['ts_pp_html'] = array(
    '#type' => 'checkbox',
    '#title' => t('Provide some page variables to html.tpl.php'),
    '#description' => t('Provides $messages, $tabs and $breadcrumb'),
    '#default_value' => variable_get('ts_pp_html'),
  );
  $form['pp']['ts_pp_regions'] = array(
    '#type' => 'checkbox',
    '#title' => t('Provide some region/page variables to all preprocesses'),
    '#description' => t('Provides $is_front and $has_{$region_name}'),
    '#default_value' => variable_get('ts_pp_regions'),
  );
  $form['pp']['ts_pp_field_vars'] = array(
    '#type' => 'checkbox',
    '#title' => t('Provide easy field variables to templates'),
    '#description' => t('Provides $field_name["rendered"], $field_name[$delta]["rendered"] and $field_name[$delta]["entity"] (for reference fields)'),
    '#default_value' => variable_get('ts_pp_field_vars'),
  );
  $form['pp']['ts_pp_entity_type'] = array(
    '#type' => 'checkbox',
    '#title' => t('Preprocess per entity type'),
    '#description' => t('Enable preprocess hooks per entity type'),
    '#default_value' => variable_get('ts_pp_entity_type'),
  );
  $form['pp']['ts_pp_views_rows'] = array(
    '#type' => 'checkbox',
    '#title' => t('Provide $rows_array & $groups_array to views.tpl.php'),
    '#description' => '$rows_array = array($delta => array(\'raw\' => array($field_name => $field_values), \'rendered\' => string)).'
      . '<br />' . '$groups_array = array($delta => array(\'title\' => string, \'rows\' => array($delta => array(\'raw\', \'rendered\')), \'grouping\' => array($grouping_info))).',
    '#default_value' => variable_get('ts_pp_views_rows'),
  );

  // Form element options
  $form['fe'] = array(
    '#type' => 'fieldset',
    '#title' => t('Form elements'),
  );

  $form['fe']['ts_label_html'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow HTML in field labels'),
    '#default_value' => variable_get('ts_label_html'),
  );

  $form['fe']['ts_title_display_ui'] = array(
    '#type' => 'checkbox',
    '#title' => t('Provide field UI to set #title_display per field'),
    '#default_value' => variable_get('ts_title_display_ui'),
  );

  $form['fe']['ts_container_description'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable #description for container elements'),
    '#default_value' => variable_get('ts_container_description'),
  );

  $element_types = array(
    'radio' => 'Radio',
    'checkbox' => 'Checkbox',
  );

  $form['fe']['ts_input_in_label'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Render inputs inside their label'),
    '#default_value' => variable_get('ts_input_in_label'),
    '#options' => $element_types,
  );

  foreach ($element_types as $type => $title) {
    $form['fe']['ts_format_' . $type] = array(
      '#type' => 'textfield',
      '#title' => t('Format string for rendering ' . $type . ' element widgets'),
      '#description' => t('Leave empty for default behaviour. String can include HTML, and placeholders: !field, !label, !field_prefix, !field_suffix, !label_prefix, !label_suffix'),
      '#default_value' => variable_get('ts_format_' . $type),
    );
  }

  // Form labels
  $form['fe']['ts_form_labels'] = array(
    '#type' => 'textarea',
    '#title' => t('Form labels'),
    '#description' => t('One per line, in the form: form_id|key|label|title_display(optional)|placeholder(optional)'),
    '#default_value' => variable_get('ts_form_labels'),
  );

  // Menu options
  $form['menu'] = array(
    '#type' => 'fieldset',
    '#title' => t('Menu options'),
  );
  $form['menu']['ts_menu_enforce_expand'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enforce menu items as expanded'),
    '#default_value' => variable_get('ts_menu_enforce_expand'),
  );
  $options = menu_parent_options(menu_get_menus(), 0);
  $form['menu']['ts_search_form_mlid'] = array(
    '#type' => 'select',
    '#title' => t('Search form in menu'),
    '#description' => t('Embed the search form within a menu item'),
    '#default_value' => variable_get('ts_search_form_mlid'),
    '#options' => array_merge(array('0' => t('- None -')), $options),
  );

  // Strip CSS options
  $form['css'] = array(
    '#type' => 'fieldset',
    '#title' => t('CSS'),
  );
  $form['css']['ts_exclude_css'] = array(
    '#type' => 'textarea',
    '#default_value' => variable_get('ts_exclude_css', 'system|system.menus.css'),
  );

  return system_settings_form($form);
}
